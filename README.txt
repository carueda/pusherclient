A simple pusher.com REST client in java

This code is a simplified version of Play-Pusher but here using Apache HttpClient 
instead of the Play framework.

Usage is straightforward. See the references and the code for more details.

Executing the demo program:
  mvn exec:java -Dexec.mainClass=pusherclient.Main -Dexec.args="appId key secret channel event message [message ...]"
Of course you need to provide your Pusher API details and have some subscribers (or use the Pusher debug utility)
to see the full effect.
  
Thank you Regis Bamba [4] for making this task much easier ;)


[1] https://github.com/rhb9/Play-Pusher
[2] http://hc.apache.org/httpcomponents-client-ga/index.html
[3] http://www.playframework.org/
[4] https://plus.google.com/100957460389037677883/posts/MSP6UimXifx



Change log

2011-08-20 first version.
