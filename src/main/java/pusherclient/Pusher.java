package pusherclient;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

/**
 * Adapted from <a href="https://github.com/rhb9/Play-Pusher">Play-Pusher</a>
 * but here using <a
 * href="http://hc.apache.org/httpcomponents-client-ga/index.html">Apache
 * HttpClient</a>.
 * 
 * @see http://pusher.com/
 * @author carueda
 */
public class Pusher {

    private static final String PUSHER_HOST = "api.pusherapp.com";
    private static final String DEFAULT_CONTENT_TYPE = "text/plain";
    private static final String DEFAULT_CHANNEL = "test_channel";
    private static final String DEFAULT_SOCKET_ID = null;

    private final String appId;
    private final String key;
    private final String secret;

    private String contentType = DEFAULT_CONTENT_TYPE;
    private String channel = DEFAULT_CHANNEL;
    private String socketId = DEFAULT_SOCKET_ID;

    public Pusher(String appId, String key, String secret) {
        this.appId = appId;
        this.key = key;
        this.secret = secret;
    }

    public Pusher contentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    public String contentType() {
        return contentType;
    }

    public Pusher channel(String channel) {
        this.channel = channel;
        return this;
    }

    public String channel() {
        return channel;
    }

    public Pusher socketId(String socketId) {
        this.socketId = socketId;
        return this;
    }

    public String socketId() {
        return socketId;
    }

    /**
     * Triggers an event on the channel() and delivers a message of the
     * contentType() to all the subscribers excluding the socketId() (if not
     * null).
     * 
     * @param event
     * @param message
     * @return server response
     * @throws Exception
     */
    public Response trigger(String event, String message) throws Exception {

        String path = "/apps/" + appId + "/channels/" + channel + "/events";

        String query = "auth_key=" + key + "&auth_timestamp=" + (System.currentTimeMillis() / 1000)
                + "&auth_version=1.0" + "&body_md5=" + PusherUtil.md5(message) + "&name=" + event
                + (socketId != null ? "&socket_id=" + socketId : "");

        String signature = PusherUtil.sha256("POST\n" + path + "\n" + query, secret);

        String uri = "http://" + PUSHER_HOST + path + "?" + query + "&auth_signature=" + signature;

        return _doPost(uri, message, contentType());
    }

    /**
     * Does the HTTP POST request.
     */
    private static Response _doPost(String uri, String message, String contentType) throws Exception {
        HttpClient httpclient = new DefaultHttpClient();
        try {
            HttpPost httppost = new HttpPost(uri);

            StringEntity reqEntity = new StringEntity(message);
            reqEntity.setContentType(contentType);

            httppost.setEntity(reqEntity);

            HttpResponse httpResponse = httpclient.execute(httppost);
            HttpEntity resEntity = httpResponse.getEntity();

            StatusLine sl = httpResponse.getStatusLine();

            Response response = new Response();
            response.setStatusCode(sl.getStatusCode());
            response.getReasonPhrase(sl.getReasonPhrase());

            EntityUtils.consume(resEntity);

            return response;

        }
        finally {
            httpclient.getConnectionManager().shutdown();
        }
    }

}
