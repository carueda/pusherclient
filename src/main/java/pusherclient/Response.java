package pusherclient;

public class Response {

    private int statusCode;
    private String reasonPhrase;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int status) {
        this.statusCode = status;
    }

    public String getReasonPhrase() {
        return reasonPhrase;
    }

    public void getReasonPhrase(String reasonPhrase) {
        this.reasonPhrase = reasonPhrase;
    }

}
