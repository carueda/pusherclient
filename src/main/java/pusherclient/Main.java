package pusherclient;

/**
 * Simple demo program.
 * 
 * @param args
 *            <pre>
 * appId key secret channel event message [message ...]
 * </pre>
 */
public class Main {
    public static void main(String[] args) throws Exception {

        if (args.length < 6) {
            System.out.println("At least six arguments expected: appId key secret channel event message [message ...]");
            return;
        }

        int ii = 0;
        String appId = args[ii++];
        String key = args[ii++];
        String secret = args[ii++];

        String channel = args[ii++];
        String event = args[ii++];

        Pusher pusher = new Pusher(appId, key, secret).channel(channel);

        for (; ii < args.length; ii++) {
            String message = args[ii];
            System.out.printf("Press enter to push: '%s'%n", message);
            System.in.read();
            Response response = pusher.trigger(event, message);
            System.out.printf("Server response: %d: %s%n%n", response.getStatusCode(), response.getReasonPhrase());
        }
    }
}
